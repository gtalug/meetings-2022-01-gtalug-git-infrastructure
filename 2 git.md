## GIT
- created by Linus Torvalds to manage the Linux Kernel source code tree
- inspired by a long line of previous systems
- the design is subtle and elegant but not intuitive to most people
- subsequently a lot of features have been bolted on

Git manages history and change for a tree of text files.
- A tree, along with git's control files, is called a "repository".
- git's control files are unobtrusively hidden inside a subdirectory ".git" and a file ".gitignore"

Git supports distributed repositories.
- There can be several different copies of a project, each in its own repository.
- creating a copy ("cloning") is easy
- copying changes from between repositories varies in intrinsic difficulty.

