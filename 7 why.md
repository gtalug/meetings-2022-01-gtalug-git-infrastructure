Why do we go through all this crazy tooling?

When something gets complicated, or its development takes time, or several people work on it, it is important to keep track of changes.  What, who, when, why.

Details matter when computers are involved.

When people work together, conflicts need to be discovered and dealt with.  Conflicts often manifest as git merge conflicts.
