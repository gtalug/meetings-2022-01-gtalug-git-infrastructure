# using a remote git repository
Broadly, there are two ways.

## using a remote git repo as an upstream
- Typically you have cloned a remote repo and consider it to be the true/main/official/upstream repository.
- you can copy any upstream changes into your repository using git-pull(1)
- if you have the right, you can copy local changes upstream using git-push(1)
- you can copy local changes upstream by asking someone with the right to do a git-pull(1) on your behalf (you are making a "pull request")

## using a remote git repo directly
- gitlab.com (and github.com) provide a free level of git project hosting
- they have become the main way of distributing open-source code
- they provide a web GUI interface so your editing, git operations, etc. can be done directly on their site
- they provide many useful ancilliary facilities (eg. bug tracking, continuous integration)
- alternatively, you can easily a local repo with gitlab or github providing hosting for an upstream repo.
