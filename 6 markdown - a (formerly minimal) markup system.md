# markdown
- a markup language is a system of annotations to modify the formatting of text https://en.wikipedia.org/wiki/Markup_language
- the term comes from the annotations an editor makes on a manuscript
- examples: troff, LaTex, SGML, HTML
- markdown was created as a radically simple markup https://en.wikipedia.org/wiki/Markdown
- unfortunately there are many variants
- gitlab treats any file with the suffix .md as a markdown document and displays the formatted version
- see https://docs.gitlab.com/ee/user/markdown.html
- this presentation was created in gitlab using a tiny subset of markdown
