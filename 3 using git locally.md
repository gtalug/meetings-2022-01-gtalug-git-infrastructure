The purest way of using git is to use it on your own computer with a local repository, using command-line tools.

Read git(1).  It tells you to read gittutorial(7), and giteveryday(7).

Important life-cycle operations:

- create a new repo: git-init(1)

- or clone an existing repo: git-clone(1)
____
    git clone git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
____
- make changes to the local files
- add files and inform git with git-add(1)
- delete files and inform git with git-rm(1)
- tell git to note changes (and explanation) with git-commit(1)
